var keyboard_en = [
    [['`', 1, 1], ['1', 1, 1], ['2', 1, 2], ['3', 1, 3], ['4', 1, 4], ['5', 1, 4], ['6', 1, 4], ['7', 1, 7], ['8', 1, 8], ['9', 1, 9], ['0', 1, 10], ['-', 1, 10], ['=', 1, 10], ['&larr;', 2.0, 10]], 
    [['Tab', 1.7, 1], ['Q', 1, 1], ['W', 1, 2], ['E', 1, 3], ['R', 1, 4], ['T', 1, 4], ['Y', 1, 7], ['U', 1, 7], ['I', 1, 8], ['O', 1, 9], ['P', 1, 10], ['[', 1, 10], [']', 1, 10], ['\\ |', 1.28, 10]], 
    [['CapsLock', 2.33, 1], ['A', 1, 1], ['S', 1, 2], ['D', 1, 3], ['F', 1, 4], ['G', 1, 4], ['H', 1, 7], ['J', 1, 7], ['K', 1, 8], ['L', 1, 9], [';', 1, 10], ['\'', 1, 10], ['Enter', 1.72, 10]], 
    [['Shift', 2.6, 1], ['Z', 1, 1], ['X', 1, 2], ['C', 1, 3], ['V', 1, 4], ['B', 1, 4], ['N', 1, 7], ['M', 1, 7], [',', 1, 8], ['.', 1, 9], ['/', 1, 10], ['Shift', 2.48, 10]], 
    [['Ctrl', 1.3, 1], ['', 1.3, 1], ['Alt', 1.3, 1], ['Space', 6.09, 6], ['Alt', 1.3, 10], ['', 1.3, 10], ['', 1.3, 10], ['Ctrl', 1.3, 10]]
];
// var keyboard_ru = [
//     [['Ё', 1], ['1', 1], ['2', 1], ['3', 1], ['4', 1], ['5', 1], ['6', 1], ['7', 1], ['8', 1], ['9', 1], ['0', 1], ['-', 1], ['=', 1], ['\\', 1], ['<-', 1]], 
//     [['Tab', 4.93], ['Й', 1], ['Ц', 1], ['У', 1], ['К', 1], ['Е', 1], ['Н', 1], ['Г', 1], ['Ш', 1], ['Щ', 1], ['З', 1], ['Х', 1], ['Ъ', 1]], 
//     [['CapsLock', 2.33], ['Ф', 1], ['Ы', 1], ['В', 1], ['А', 1], ['П', 1], ['Р', 1], ['О', 1], ['Л', 1], ['Д', 1], ['Ж', 1], ['Э', 1], ['Enter', 3]], 
//     [['Shift', 3.75], ['Я', 1], ['Ч', 1], ['С', 1], ['М', 1], ['И', 1], ['Т', 1], ['Ь', 1], ['Б', 1], ['Ю', 1], ['.', 1], ['Shift', 3.75]], 
//     [['Ctrl', 1.8], ['', 1], ['Alt', 1.416], ['Space', 8.4], ['Alt', 1.416], ['', 1], ['Menu', 1.416], ['Ctrl', 1.8]]
// ];

var char_to_key =  {'`': [1, 1], '1': [1, 2], '2': [1, 3], '3': [1, 4], 
    '4': [1, 5], '5': [1, 6], '6': [1, 7], '7': [1, 8], '8': [1, 9], 
    '9': [1, 10], '0': [1, 11], '-': [1, 12], '=': [1, 13], '\\': [1, 14],
    'BACKSPACE': [1, 15], 'Q': [2, 2], 'W': [2, 3], 'E': [2, 4],
    'R': [2, 5], 'T': [2, 6], 'Y': [2, 7], 'U': [2, 8], 'I': [2, 9],
    'O': [2, 10], 'P': [2, 11], '[': [2, 12], ']': [2, 13], 'CapsLock': [3, 1],
    'A': [3, 2], 'S': [3, 3], 'D': [3, 4], 'F': [3, 5], 'G': [3, 6],
    'H': [3, 7], 'J': [3, 8], 'K': [3, 9], 'L': [3, 10], ';': [3, 11], 
    '"': [3, 12], 'Enter': [3, 13], 'Shift': [4, 1], 'Z': [4, 2], 'X': [4, 3],
    'C': [4, 4], 'V': [4, 5], 'B': [4, 6], 'N': [4, 7], 'M': [4, 8], 
    ',': [4, 9], '.': [4, 10], '/': [4, 11], 'Shift': [4, 12], ' ': [5, 4]};

var keyboard = keyboard_en;

$(function() {
    generate_helper();
});

function generate_helper (argument) {
    var base_key_width = parseInt($('<div class="key"></p>').hide().appendTo("body")
                             .css("width").replace("px", ""));

    html = '<div id="keyboard">';
    for (var i = 0; i < keyboard.length; i++) {
        html += '<div class="keyrow" id="keyrow' + i + '">';
        for (var j = 0; j < keyboard[i].length; j++) {
            var key_width = Math.round(keyboard[i][j][1] * base_key_width);
            keyboard[i][j];
            html += '<div class="key" style="width: ' + key_width + 'px">';
            html += keyboard[i][j][0] + '</div>';
            //html += '</div>';
        }
        html += '</div>';
    }
    html += '</div>';
    html += '<div id="hands"><div id="left_hand"><div></div></div><div id="hand_spacer">'
    html += '</div><div id="right_hand"><div></div></div></div>';
    $('body').append(html);
}

function set_helper_expect(char_key) {
    var key_pos = char_to_key[char_key.toUpperCase()];
    $('#left_hand > div, #right_hand > div').css('background', 'none');
    $('.key').css('background', 'white');
    $($('.keyrow')[key_pos[0] - 1].childNodes[key_pos[1] - 1]).css('background', '#41A7FC');
    var finger = keyboard[key_pos[0] - 1][key_pos[1] - 1][2];
    if (finger < 6) {
        $('#left_hand > div').css('background', 'url("img/' + String(finger) + '.png")');
    }
    else {
        $('#right_hand > div').css('background', 'url("img/' + String(finger) + '.png")');
    }
    $()

}

function check_input(user_input, expected_input) {
    //TODO: collect statistics here
    if ($.inArray(user_input, expected_input) < 0) {
        return false;
    }
    else {
        return true;
    }
}
