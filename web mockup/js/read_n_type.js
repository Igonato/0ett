// I order to not spoil to much of a text and make typing uninteresting
// we show only limited amount of letters after the place we currently are
var LETTERS_PRESHOW = 30;
// test text
var text = "In a hole in the ground there lived a hobbit. Not a nasty, dirty, \
wet hole, filled with the ends of worms and an oozy smell, nor yet a dry, \
bare, sandy hole with nothing in it to sit down on or to eat: it was a \
hobbit-hole, and that means comfort. \
It had a perfectly round door like a porthole, painted green, with a shiny \
yellow brass knob in the exact middle. The door opened on to a tube-shaped \
hall like a tunnel: a very comfortable tunnel without smoke, with panelled \
walls, and floors tiled and carpeted, provided with polished chairs, and lots \
and lots of pegs for hats and coats - the hobbit was fond of visitors. The \
tunnel wound on and on, going fairly but not quite straight into the side of \
the hill - The Hill, as all the people for many miles round called it - and \
many little round doors opened out of it, first on one side and then on \
another. No going upstairs for the hobbit: bedrooms, bathrooms, cellars, \
pantries (lots of these), wardrobes (he had whole rooms devoted to clothes), \
kitchens, dining-rooms, all were on the same floor, and indeed on the same \
passage. The best rooms were all on the left-hand side (going in), for these \
were the only ones to have windows, deep-set round windows looking over his \
garden and meadows beyond, sloping down to the river.";

var cursor_position = 0;
var preshow_position = 0;

var event1;

$(function() {
    // undind backspace
    $(document).unbind('keydown').bind('keydown', function (event) {
        var doPrevent = false;
        if (event.keyCode === 8) {
            var d = event.srcElement || event.target;
            if ((d.tagName.toUpperCase() === 'INPUT' && (d.type.toUpperCase() === 'TEXT' || d.type.toUpperCase() === 'PASSWORD')) 
                 || d.tagName.toUpperCase() === 'TEXTAREA') {
                doPrevent = d.readOnly || d.disabled;
            }
            else {
                doPrevent = true;
            }
        }

        if (doPrevent) {
            event.preventDefault();
        }
    });

    $("html").keypress(keypress);
    $("html").keyup(backspace);
    render_preshow();
    set_helper_expect(text[cursor_position]);
});

function keypress(e) { 
    if (e.which == 13 || e.which == 8) {
        e.preventDefault();
        return false;
    }
    if (e.which == 32) {
        e.preventDefault();
    }

    var user_input = String.fromCharCode(e.charCode);
    if (check_input(user_input, [text[cursor_position]])) {
        $($('#text span')[cursor_position]).fadeTo(0, 1);
    } 
    else {
        if (user_input == ' ') {
            user_input = '&nbsp;';
        }
        $($('#text span')[cursor_position]).addClass('error').html(user_input);
    }
    cursor_position++;
    render_preshow();
    set_helper_expect(text[cursor_position]);
}
function backspace(e) {
    if (e.keyCode != 8 || cursor_position == 0) return;
    cursor_position -= 1; 
    $($('#text span')[cursor_position]).removeClass('error')
        .html(text[cursor_position]);
    render_preshow();
    set_helper_expect(text[cursor_position]);
}
function render_preshow() {
    // preshow position - cursor position === current preshowed letters count
    while (preshow_position - cursor_position < LETTERS_PRESHOW) {
        $('#text').append('<span style="opacity: 0.0">' + 
                          text[preshow_position++] + '</span>');
    } 
    var letters_to_fade = $('#text span').slice(cursor_position, preshow_position);
    for (var i = 0; i < letters_to_fade.length; i++) {
        var opacity = 0.5 * ((LETTERS_PRESHOW - i) / LETTERS_PRESHOW) + 0.1;
        $(letters_to_fade[i]).fadeTo(0, opacity);
    };

}